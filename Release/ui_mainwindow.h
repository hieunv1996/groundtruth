/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>
#include <myqlabel.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGroupBox *grViewImage;
    MyQLabel *lbViewImage;
    QPushButton *btnOK;
    QTextEdit *txtPlateNumber;
    QLabel *label;
    QToolButton *btnBrowser;
    QTextEdit *txtPlatePosition;
    QLabel *label_2;
    QTextEdit *txtFrameID;
    QLabel *label_3;
    QPushButton *btnNextFrame;
    QPushButton *btnChooseFile;
    QLabel *lbNoti;
    QTextEdit *txtClipboard;
    QLabel *label_4;
    QTextEdit *txtFIleLocation;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(1301, 655);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        grViewImage = new QGroupBox(centralWidget);
        grViewImage->setObjectName(QStringLiteral("grViewImage"));
        grViewImage->setGeometry(QRect(10, 0, 951, 621));
        grViewImage->setAlignment(Qt::AlignCenter);
        lbViewImage = new MyQLabel(grViewImage);
        lbViewImage->setObjectName(QStringLiteral("lbViewImage"));
        lbViewImage->setGeometry(QRect(0, 30, 941, 581));
        lbViewImage->setMouseTracking(true);
        lbViewImage->setScaledContents(true);
        lbViewImage->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lbViewImage->setWordWrap(true);
        btnOK = new QPushButton(centralWidget);
        btnOK->setObjectName(QStringLiteral("btnOK"));
        btnOK->setGeometry(QRect(970, 320, 171, 81));
        txtPlateNumber = new QTextEdit(centralWidget);
        txtPlateNumber->setObjectName(QStringLiteral("txtPlateNumber"));
        txtPlateNumber->setGeometry(QRect(970, 270, 171, 31));
        txtPlateNumber->setAutoFormatting(QTextEdit::AutoNone);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(970, 250, 101, 17));
        btnBrowser = new QToolButton(centralWidget);
        btnBrowser->setObjectName(QStringLiteral("btnBrowser"));
        btnBrowser->setGeometry(QRect(970, 20, 241, 31));
        txtPlatePosition = new QTextEdit(centralWidget);
        txtPlatePosition->setObjectName(QStringLiteral("txtPlatePosition"));
        txtPlatePosition->setGeometry(QRect(970, 150, 241, 81));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(970, 130, 111, 17));
        txtFrameID = new QTextEdit(centralWidget);
        txtFrameID->setObjectName(QStringLiteral("txtFrameID"));
        txtFrameID->setEnabled(false);
        txtFrameID->setGeometry(QRect(970, 80, 241, 31));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(970, 60, 91, 17));
        btnNextFrame = new QPushButton(centralWidget);
        btnNextFrame->setObjectName(QStringLiteral("btnNextFrame"));
        btnNextFrame->setGeometry(QRect(970, 420, 171, 81));
        btnChooseFile = new QPushButton(centralWidget);
        btnChooseFile->setObjectName(QStringLiteral("btnChooseFile"));
        btnChooseFile->setGeometry(QRect(970, 580, 321, 31));
        lbNoti = new QLabel(centralWidget);
        lbNoti->setObjectName(QStringLiteral("lbNoti"));
        lbNoti->setGeometry(QRect(970, 510, 321, 31));
        QFont font;
        font.setUnderline(true);
        lbNoti->setFont(font);
        lbNoti->setAutoFillBackground(true);
        lbNoti->setAlignment(Qt::AlignCenter);
        lbNoti->setWordWrap(true);
        txtClipboard = new QTextEdit(centralWidget);
        txtClipboard->setObjectName(QStringLiteral("txtClipboard"));
        txtClipboard->setGeometry(QRect(1160, 270, 131, 231));
        txtClipboard->setAutoFormatting(QTextEdit::AutoNone);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(1160, 240, 101, 17));
        txtFIleLocation = new QTextEdit(centralWidget);
        txtFIleLocation->setObjectName(QStringLiteral("txtFIleLocation"));
        txtFIleLocation->setGeometry(QRect(970, 550, 321, 21));
        txtFIleLocation->setAutoFormatting(QTextEdit::AutoNone);
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        grViewImage->setTitle(QApplication::translate("MainWindow", "Frame Viewer", Q_NULLPTR));
        lbViewImage->setText(QApplication::translate("MainWindow", "Image view here", Q_NULLPTR));
        btnOK->setText(QApplication::translate("MainWindow", "Add Plate", Q_NULLPTR));
        txtPlateNumber->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif'; font-size:9pt;\"><br /></p></body></html>", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Plate Number", Q_NULLPTR));
        btnBrowser->setText(QApplication::translate("MainWindow", "Browser video", Q_NULLPTR));
        txtPlatePosition->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt;\">X</span><span style=\" font-family:'Sans Serif'; font-size:9pt; vertical-align:sub;\">1</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"> = 0</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt;\">Y</span><span style=\" font-family:'Sans Serif'; font-size:9pt; vertical-align:sub;\">1</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"> = 0</spa"
                        "n></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt;\">X</span><span style=\" font-family:'Sans Serif'; font-size:9pt; vertical-align:sub;\">2</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"> = 0</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt;\">Y</span><span style=\" font-family:'Sans Serif'; font-size:9pt; vertical-align:sub;\">2</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"> = 0</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Plate Position", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Frame ID", Q_NULLPTR));
        btnNextFrame->setText(QApplication::translate("MainWindow", "Next Frame", Q_NULLPTR));
        btnChooseFile->setText(QApplication::translate("MainWindow", "Choose file location(Data will save append)", Q_NULLPTR));
        lbNoti->setText(QApplication::translate("MainWindow", "Notification", Q_NULLPTR));
        txtClipboard->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif'; font-size:9pt;\"><br /></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Clipboard", Q_NULLPTR));
        txtFIleLocation->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif'; font-size:9pt;\"><br /></p></body></html>", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
