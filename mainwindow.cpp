#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myqlabel.h"
#include <algorithm>
using namespace std;
#include <functional>
#include <cctype>
#include <locale>


Plate _plateOld;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);
    ui->radioEdit->setChecked(false);
    connect(ui->lbViewImage,SIGNAL(Mouse_Pressed()),this,SLOT(mouse_presses()));
    connect(ui->lbViewImage,SIGNAL(Mouse_Pos()),this,SLOT(mouse_current_pos()));
    connect(ui->lbViewImage,SIGNAL(Mouse_Leave()),this,SLOT(mouse_leave()));
}

bool MainWindow::frameExtract(){
    bool success = false;
    if(cap.isOpened()){
        success = cap.read(frame);
        if(success){
            setLabelPixmap(frame);
//            cvtColor(frame,frame,CV_BGR2RGB);
//            ui->lbViewImage->setPixmap(QPixmap::fromImage(
//                                           QImage(frame.data,frame.cols,frame.rows,
//                                                  frame.step,QImage::Format_RGB888)));
            scaleX = (float)frame.cols/ui->lbViewImage->width();
            scaleY = (float)frame.rows/ui->lbViewImage->height();
        }else{
            ui->lbViewImage->setText(ui->lbViewImage->text() + "\n" + "Can not read frame !");
        }
    }else{
        ui->lbViewImage->setText("Open video fail!");
    }
    return success;
}

void MainWindow::toFrameID(int _frameID)
{
    bool isSuccess = cap.set(CV_CAP_PROP_POS_FRAMES,_frameID);
    if(isSuccess){
        frameID = _frameID + 1;
        char* textFrameID = new char[10];
        sprintf(textFrameID,"%d",frameID);
        ui->txtFrameID->setText(textFrameID);

        frameExtract();
    }
}

QString MainWindow::platePredict(int xPos, int yPos)
{
    QString result = "";

    if(vtPrePlate.size() == 0) return result;

    int xMin = DISTANCE_MAX,yMin = DISTANCE_MAX,x,y;
    for(unsigned i = 0;i < vtPrePlate.size();i++){

        if(vtPrePlate[i].getFrameID().toInt() != frameID -1) continue;

        x = abs(vtPrePlate[i].getTopLeftX() - xPos);
        y = abs(vtPrePlate[i].getTopLeftY() - yPos);

        if(x < xMin && y < yMin){
            xMin = x;
            yMin = y;
            result = vtPrePlate[i].getPlateNumber();
        }
    }
    return result;
}

void MainWindow::on_btnBrowser_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(),
                                                    tr("Video files (*.mp4 *.avi)"));
    if(fileName != ""){
       string videoPath = fileName.toStdString();
        this->cap.open(videoPath);
        if(frameExtract()){
            //reset value
            frameID = 1;
            //            lstPlateInfo.clear();
            char* textFrameID = new char[10];
            sprintf(textFrameID,"%d",frameID);
            ui->txtFrameID->setText(textFrameID);
            ui->txtPlatePosition->setText("");
        }
    }else{
        ui->lbViewImage->setText("Error opening video file");
    }
}

void MainWindow::on_btnOK_clicked()
{
    QString filePath = ui->txtFIleLocation->toPlainText();
    if(filePath == ""){
        msgBox.setText("Select file to save first");
        msgBox.exec();
        return;
    }
    QString frameID = ui->txtFrameID->toPlainText();
    QString plateNumber = ui->txtPlateNumber->toPlainText().toUpper();
    if(frameID == ""){
        msgBox.setText("Select video first");
        msgBox.exec();
        return;
    }
    if(plateNumber == ""){
        msgBox.setText("Plate number is empty");
        msgBox.exec();
        return;
    }
    QString position = ui->txtPlatePosition->toPlainText();
    if(position == ""){
        msgBox.setText("Position is empty");
        msgBox.exec();
        return;
    }

    position.replace(QRegExp("[^0-9\\n]"),"");
    position.replace(QRegExp("\\n"),",");
    plate.setFrameID(frameID);
    plate.setPlateNumber(plateNumber);
    plate.setPosition(position);
    fileWriterAppend(plate,filePath);
    ui->txtPlatePosition->setText("");
    ui->lbNoti->setText("Clicked Add Plate button");

    vtPrePlate.push_back(plate);

    //remove plateNumber
    ui->txtPlateNumber->setText("");
}

void MainWindow::on_btnNextFrame_clicked()
{
    if(frameExtract()){
        frameID++;
        char* textFrameID = new char[10];
        sprintf(textFrameID,"%d",frameID);
        ui->txtFrameID->setText(textFrameID);
        ui->lbNoti->setText("Clicked Next Frame button");

        for(unsigned i = 0;i < vtPrePlate.size();++i){
            if(vtPrePlate[i].getFrameID().toInt() == frameID - 2){
                vtPrePlate.erase(vtPrePlate.begin() + i);
                i--;
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::mouse_current_pos()
{
    //    ui->txtPlatePosition->setText("Mouse move");
}

void MainWindow::mouse_presses()
{
    if(frame.data){
        setLabelPixmap(frame);
        plate.setTopLeftX(ui->lbViewImage->x1*scaleX);
        plate.setTopLeftY(ui->lbViewImage->y1*scaleY);
    }
    ui->txtPlatePosition->setText(QString("TopLeftX = %1\nTopLeftY = %2")
                                  .arg(((int)(ui->lbViewImage->x1*scaleX))).arg((int)(ui->lbViewImage->y1*scaleY)));
}

void MainWindow::mouse_leave()
{
    QString text = ui->txtPlatePosition->toPlainText();

    ui->txtPlatePosition->setText(text+"\n"+QString("BottomRightX = %1\nBottomRightY = %2")
                                  .arg((int)(ui->lbViewImage->x2*scaleX)).arg((int)(ui->lbViewImage->y2*scaleY)));
    if(frame.data){
        // draw rectangle in Mat
        Mat _frame = frame.clone();
        rectangle(_frame,Point(ui->lbViewImage->x1*scaleX,ui->lbViewImage->y1*scaleY),Point(ui->lbViewImage->x2*scaleX,ui->lbViewImage->y2*scaleY),Scalar(66,244,113),4,8);
        setLabelPixmap(_frame);

        //
        QString plateResult = platePredict(plate.getTopLeftX(),plate.getTopLeftY());
        if(plateResult != ""){
            ui->txtPlateNumber->setText(plateResult);
        }else{
            ui->txtPlateNumber->setText("");
            ui->txtPlateNumber->setFocus();
        }
    }
}

bool MainWindow::fileWriterAppend(Plate plate, QString filePath)
{
    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream stream(&file);
        if(!plate.getFrameID().isEmpty()){
            stream << plate.getFrameID()<<","<<plate.getPosition()<<","<<plate.getPlateNumber()<<endl;
        }
        stream.flush();
        file.close();
        return true;
    } else {

        msgBox.setText("Error writing file");
        msgBox.exec();
        return false;
    }
}

bool MainWindow::fileWriteUpdate(Plate plateOld,Plate plateNew, QString filePath,QString dir)
{
    QFile file(filePath);
    cout<<"\nFilePath:"<<filePath.toStdString()<<endl;
    QFile tempFile(dir.append("/temp"));
    //QFileInfo fileInfo(filePath);

    QString strTemp;
    tempFile.open(QIODevice::WriteOnly|QIODevice::Text);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append|QIODevice::Text|QIODevice::ReadOnly))
    {
        QTextStream stream(&file);
        QTextStream tmp(&tempFile);

        bool writeOrgLine=true;
        QString strReplace=QString("%1,%2,%3,%4,%5,%6")
                .arg(plateOld.getFrameID()).arg(plateOld.getTopLeftX()).arg(plateOld.getTopLeftY()).arg(plateOld.getBottomRightX()).arg(plateOld.getBottomRightY()).arg(plateOld.getPlateNumber());
        cout<<"\nstrReplace:"<<strReplace.toStdString()<<endl;
        QString strNew=QString("%1,%2,%3")
                .arg(plateNew.getFrameID()).arg(plateNew.getPosition()).arg(plateNew.getPlateNumber());
        cout<<"strNew:"<<strNew.toStdString()<<endl;
         file.seek(0);
        cout<<"\nstream.atEnd="<<stream.atEnd()<<endl;

        while(!stream.atEnd())
        {

            strTemp=stream.readLine();
            cout<<"strTemp:"<<strTemp.toStdString()<<endl;
            if(QString::compare(strTemp,strReplace, Qt::CaseInsensitive)==0)
            {
                writeOrgLine=false;
                //cout<<"strTemp:"<<strTemp.toStdString()<<endl;


            }else
            {
                writeOrgLine=true;
               // tmp<<strTemp<<"\n";
            }

            if(writeOrgLine)
                tmp<<strTemp<<"\n";
            else
                {
                strTemp=strNew;
                tmp<<strTemp<<"\n";
            }


        }
        file.remove();
        tempFile.rename(filePath);

        msgBox.setText("Update file  successfully!");
         msgBox.exec();
        return true;
    } else {

        msgBox.setText("Error updating file");
        msgBox.exec();
        return false;
    }

}


void MainWindow::on_btnChooseFile_clicked()
{
    if(ui->txtFrameID->toPlainText() == ""){
        msgBox.setText("Open video file first");
        msgBox.exec();
        return;
    }
    QString fileName = QFileDialog::getOpenFileName(this, tr("Save File"), QString(),
                                                    tr("Plane text file (*)"));
    if (!fileName.isEmpty()) {
        ui->txtFIleLocation->setText(fileName);

        QFile inputFile(fileName);
        if(inputFile.size() == 0) return;
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QTextStream in(&inputFile);
            QString line = "";
            while (!in.atEnd())
            {
                line = in.readLine();
            }
            if(line.isNull() || line.isEmpty()){
                return;
            }else{
                QString frameNumber = "";
                for(int i = 0;i < line.size();++i){
                    if(line[i] == ',') break;
                    if(line[i] >= '0' && line[i] <= '9') frameNumber += line[i];
                }
                toFrameID(frameNumber.toInt());
            }
            inputFile.close();
        }
    }
}

void MainWindow::setLabelPixmap(Mat img)
{
    cvtColor(img,img,CV_BGR2RGB);
    ui->lbViewImage->setPixmap(QPixmap::fromImage(
                                   QImage(img.data,img.cols,img.rows,
                                          img.step,QImage::Format_RGB888)));
}


// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

std::string splitFrameID(QString s, char delimiter) {
   // string internal;
    string str=s.toStdString();
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    if(getline(ss, tok, delimiter)) {
      return trim(tok);
    }


    return NULL;
}

void xoaList(vector<Plate> &v)
{
    while(!v.empty())
    {
        v.pop_back();
    }
}
void MainWindow::draw(Plate p)
{
  QString frameID=p.getFrameID();
  int topLeftX=p.getTopLeftX();
  int topLeftY=p.getTopLeftY();
  int bottomRightX=p.getBottomRightX();
  int bottomRightY=p.getBottomRightY();
  QString plateNumber=p.getPlateNumber();
  ui->txtFrameID->setText(frameID);
  ui->txtPlatePosition->setText(QString("TopLeftX=%1\nTopLeftY=%2\nBottomRightX = %3\nBottomRightY = %4")
                               .arg(topLeftX) .arg(topLeftY)
                                .arg(bottomRightX).arg(bottomRightY));
  ui->txtPlateNumber->setText(plateNumber);
  cout<<"\nInfo:"<<ui->txtFrameID->toPlainText().toStdString()<<","<<ui->txtPlatePosition->toPlainText().toStdString()<<", "<<ui->txtPlateNumber->toPlainText().toStdString();
   if(frame.data)
   {
   Mat _frame = frame.clone();
   rectangle(_frame,Point(topLeftX,topLeftY),
             Point(bottomRightX,bottomRightY),
             Scalar(0,255,255),4,8);//rgb vang 0 255 255
   setLabelPixmap(_frame);
   }
}
map<QString,vector<Plate> > getListPlates(QString fileName)
{
    std::map<QString,vector<Plate> > mPlates;
    int dem=0;
    string idFrameCur,idFramePre;
    vector<Plate> list2;
    QFile inputFile(fileName);
//    if(inputFile.size() == 0) return;
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        QString line = "";

        while (!in.atEnd())
        {
            line = in.readLine();
            dem++;
            if(line!=NULL)
            {
                idFrameCur=splitFrameID(line,',') ;
                if(dem>1)
                {
                    if(atoi(idFrameCur.c_str())!=atoi(idFramePre.c_str()))
                    {
                        mPlates.insert(std::pair<QString,vector<Plate> >(QString::fromStdString(idFramePre),list2));
                        xoaList(list2);
                    }
                }
                Plate p(line);
                list2.push_back(p);
                idFramePre=idFrameCur;
             }

          }
    }
    inputFile.close();
          cout<<"mPlates: size="<<mPlates.size()<<",dem="<<dem;
          return mPlates;
}
 int nextPlate=0;
void MainWindow::on_btnSearch_clicked()
{

    if(ui->radioEdit->isChecked())
    {
        std::map<QString,vector<Plate> > mPlates;
        int dem=0;
        string idFrameCur,idFramePre;
        vector<Plate> list2;
        QString fileName = ui->txtFIleLocation->toPlainText();
        if (!fileName.isEmpty())
        {
                mPlates=getListPlates(fileName);
                  cout<<"mPlates: size="<<mPlates.size()<<",dem="<<dem;


        }
        //tim kiem frameid vua nhap vao textbox, neu tim thay thi tra ve lan luot, nguoc lai thong bao ko co

       map<QString,vector<Plate> >::iterator mapPtr;
       bool isExit=false;
       for(mapPtr=mPlates.begin(); mapPtr!=mPlates.end();mapPtr++)
       {
           if(mapPtr->first==(ui->txtFrameID->toPlainText()))
           {
               nextPlate++;
               int numberOfListPlates=mapPtr->second.size();
                msgBox.setText("Tim thay");
                msgBox.exec();
                cout<<"\nTim thay "<<(mapPtr->first).toInt()<<",so phan tu="<<numberOfListPlates<<endl;
                toFrameID(mapPtr->first.toInt()-1);
                isExit=true;

                draw(mapPtr->second.at(0));
                _plateOld=mapPtr->second.at(0);
                if(nextPlate>0 && nextPlate<numberOfListPlates)
                {
                    draw(mapPtr->second.at(nextPlate));
                     _plateOld=mapPtr->second.at(nextPlate);
                }else
                {
                    nextPlate=0;
                }
                break;
           }
           else
           {
               isExit=false;
           }
       }
       if(isExit==false)
       {
           msgBox.setText("Frame tim kiem khong co bien so!");
           msgBox.exec();
       }

    }
}

void MainWindow::on_radioEdit_clicked()
{
    if(ui->radioEdit->isChecked())
    {
    ui->txtFrameID->setEnabled(true);
    ui->txtFrameID->setText("");
    ui->txtFrameID->setFocus();
    ui->lbNoti->setText("Clicked Edit RadioButton");
    }
    else
    {
        QString fileName = ui->txtFIleLocation->toPlainText();
        if (!fileName.isEmpty()) {
            ui->txtFIleLocation->setText(fileName);

            QFile inputFile(fileName);
            if(inputFile.size() == 0) return;
            if (inputFile.open(QIODevice::ReadOnly))
            {
                QTextStream in(&inputFile);
                QString line = "";
                while (!in.atEnd())
                {
                    line = in.readLine();
                }
                if(line.isNull() || line.isEmpty()){
                    return;
                }else{
                    QString frameNumber = "";
                    for(int i = 0;i < line.size();++i){
                        if(line[i] == ',') break;
                        if(line[i] >= '0' && line[i] <= '9') frameNumber += line[i];
                    }
                    toFrameID(frameNumber.toInt());
                }
                inputFile.close();
            }
        }

    }
}



void MainWindow::on_btnUpdate_clicked()
{
    //save thong tin moi
    QString frameID=ui->txtFrameID->toPlainText();
    QString plateNumber=ui->txtPlateNumber->toPlainText();
    QString filePath = ui->txtFIleLocation->toPlainText();
    QString dir="/home/ninhttd/Documents/video";
    if(filePath == ""){
        msgBox.setText("Select file to save first");
        msgBox.exec();
        return;
    }
    if(frameID == ""){
        msgBox.setText("Select video first");
        msgBox.exec();
        return;
    }
    if(plateNumber == ""){
        msgBox.setText("Plate number is empty");
        msgBox.exec();
        return;
    }
    QString position = ui->txtPlatePosition->toPlainText();
    if(position == ""){
        msgBox.setText("Position is empty");
        msgBox.exec();
        return;
    }

    position.replace(QRegExp("[^0-9\\n]"),"");
    position.replace(QRegExp("\\n"),",");
    plate.setFrameID(frameID);
    plate.setPlateNumber(plateNumber);
    plate.setPosition(position);
    fileWriteUpdate(_plateOld,plate,filePath,dir);
    ui->txtPlatePosition->setText("");
    ui->lbNoti->setText("Clicked Update Plate button");

    vtPrePlate.push_back(plate);

    //remove plateNumber
    ui->txtPlateNumber->setText("");

}
