#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once
#include <QMainWindow>
#include <bits/stdc++.h>
#include <QFileDialog>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <QRubberBand>
#include <QWidget>
#include <QMessageBox>
#include <QTextStream>
#include <QFile>
#include <QRegExp>
#include "plate.h"

using namespace std;
using namespace cv;


#define DISTANCE_MAX 100

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool frameExtract();
    void toFrameID(int frameID);
    QString platePredict(int xPos, int yPos);
    ~MainWindow();

private slots:
    void on_btnBrowser_clicked();
    void on_btnOK_clicked();
    void on_btnNextFrame_clicked();
    void mouse_current_pos();
    void mouse_presses();
    void mouse_leave();
    bool fileWriterAppend(Plate plate, QString filePath);
    bool fileWriteUpdate(Plate plateOld,Plate plateNew,QString filePath, QString dir);
    void on_btnChooseFile_clicked();
    void setLabelPixmap(Mat img);
    void draw(Plate p);
    void on_btnSearch_clicked();

    void on_radioEdit_clicked();
    void on_btnUpdate_clicked();

private:
    Ui::MainWindow *ui;
    VideoCapture cap;
    int frameID;
    Mat frame;
    QMessageBox msgBox;
    float scaleX,scaleY;

    // storage plates in pre frames
    Plate plate;
    vector<Plate> vtPrePlate;
};

#endif // MAINWINDOW_H
