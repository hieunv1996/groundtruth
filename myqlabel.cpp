#include "myqlabel.h"

MyQLabel::MyQLabel(QWidget *parent) : QLabel(parent)
{

}

void MyQLabel::mousePressEvent(QMouseEvent *ev)
{
    this->x1 = ev->x();
    this->y1 = ev->y();
    emit Mouse_Pressed();
}

void MyQLabel::mouseMoveEvent(QMouseEvent *ev)
{
    this->x = ev->x();
    this->y = ev->y();
    emit Mouse_Pos();
}

void MyQLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    this->x2 = ev->x();
    this->y2 = ev->y();
    emit Mouse_Leave();
}
