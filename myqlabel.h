#ifndef MYQLABEL_H
#define MYQLABEL_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QDebug>

class MyQLabel : public QLabel
{
    Q_OBJECT
public:
    int x,y;
    float x1,y1,x2,y2;

    explicit  MyQLabel(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
signals:
    void Mouse_Pressed();
    void Mouse_Pos();
    void Mouse_Leave();
};

#endif // MYQLABEL_H
