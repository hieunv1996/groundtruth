#include "plate.h"
#include<vector>
#include<string>
#include <sstream>
using namespace std;

Plate::Plate(QString frameID, int topleftX, int topleftY , QString plateNumber)
{
    this->frameID = frameID;
    this->topLeftX = topleftX;
    this->topLeftY = topleftY;
    this->plateNumber = plateNumber;
}
std::vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
      internal.push_back(tok);
    }

    return internal;
}

 Plate::Plate(QString line)
 {
     string s=line.toStdString();
     vector<string> sep = split(s,',');
     this->setFrameID(QString::fromStdString(sep[0]));
     this->setTopLeftX(atoi(sep[1].c_str()));
     this->setTopLeftY(atoi(sep[2].c_str()));
     this->setBottomRightX(atoi(sep[3].c_str()));
     this->setBottomRightY(atoi(sep[4].c_str()));
     this->setPlateNumber(QString::fromStdString(sep[5]));
 }

QString Plate::getFrameID() const
{
    return frameID;
}

void Plate::setFrameID(const QString &value)
{
    frameID = value;
}

QString Plate::getPlateNumber() const
{
    return plateNumber;
}

void Plate::setPlateNumber(const QString &value)
{
    plateNumber = value;
}

QString Plate::getPosition() const
{
    return position;
}

void Plate::setPosition(const QString &value)
{
    position = value;
}

void Plate::clear()
{
    frameID = "";
    position = "";
    plateNumber = "";
    topLeftX = -1;
    topLeftY = -1;
}

int Plate::getTopLeftX() const
{
    return topLeftX;
}

void Plate::setTopLeftX(int value)
{
    topLeftX = value;
}

int Plate::getTopLeftY() const
{
    return topLeftY;
}

void Plate::setTopLeftY(int value)
{
    topLeftY = value;
}

void Plate::setBottomRightX(int value)
{
    bottomRightX=value;
}

int Plate::getBottomRightX()const
{
    return bottomRightX;
}
void Plate::setBottomRightY(int value)
{
    bottomRightY=value;
}

int Plate::getBottomRightY()const
{
    return bottomRightY;
}

Plate::Plate()
{
}
