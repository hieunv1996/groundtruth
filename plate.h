#ifndef PLATE_H
#define PLATE_H

#include <QString>
using namespace std;
class Plate
{
private:
    QString frameID;
    QString plateNumber;
    int topLeftX;
    int topLeftY;
    int bottomRightX;
    int bottomRightY;
    QString position;
public:
    Plate();
    Plate(QString line);
    Plate(QString frameID, QString position,QString plateNumber);
    Plate(QString frameID, int topleftX, int topleftY, QString plateNumber);
    QString getFrameID() const;
    void setFrameID(const QString &value);
    QString getPlateNumber() const;
    void setPlateNumber(const QString &value);
    QString getPosition() const;
    void setPosition(const QString &value);
    void clear();
    int getTopLeftX() const;
    void setTopLeftX(int value);
    int getTopLeftY() const;
    void setTopLeftY(int value);

    void setBottomRightX(int value);
    int getBottomRightX()const;
    void setBottomRightY(int value);
    int getBottomRightY()const;


};

#endif // PLATE_H
